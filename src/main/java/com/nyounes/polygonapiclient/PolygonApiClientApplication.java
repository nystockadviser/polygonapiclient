package com.nyounes.polygonapiclient;

import com.nyounes.polygonapiclient.config.model.PolygonAPIConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(PolygonAPIConfiguration.class)
public class PolygonApiClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(PolygonApiClientApplication.class, args);
    }
}
