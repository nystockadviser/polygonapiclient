package com.nyounes.polygonapiclient.config.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "io.polygon.api")
public class PolygonAPIConfiguration {
    private String authorizationToken;
    private String baseUrl;
    private String symbolDetailsPath;
    private String stockDividendsPath;
    private String stockDailyOpenClosePath;
}
