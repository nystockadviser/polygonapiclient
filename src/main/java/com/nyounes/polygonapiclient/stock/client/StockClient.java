package com.nyounes.polygonapiclient.stock.client;

import com.nyounes.polygonapiclient.stock.model.StockDailyOpenClose;
import com.nyounes.polygonapiclient.stock.model.StockDividends;
import com.nyounes.polygonapiclient.stock.model.TickerDetails;
import org.springframework.http.ResponseEntity;

import java.util.Date;

public interface StockClient {
    ResponseEntity<TickerDetails> getSymbolDetails(String symbol);
    ResponseEntity<StockDividends> getStockDividends(String symbol);
    ResponseEntity<StockDailyOpenClose> getStockDailyOpenClose(String symbol, Date date, boolean adjusted);
}
