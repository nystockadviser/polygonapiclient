package com.nyounes.polygonapiclient.stock.client.impl;

import com.nyounes.polygonapiclient.config.model.PolygonAPIConfiguration;
import com.nyounes.polygonapiclient.stock.model.StockDailyOpenClose;
import com.nyounes.polygonapiclient.stock.model.StockDividends;
import com.nyounes.polygonapiclient.stock.model.TickerDetails;
import com.nyounes.polygonapiclient.stock.client.StockClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Component
public class StockClientImpl implements StockClient {
    private PolygonAPIConfiguration polygonAPIConfiguration;
    private RestTemplate restTemplate;

    @Autowired
    public void setPolygonAPIConfiguration(PolygonAPIConfiguration polygonAPIConfiguration) {
        this.polygonAPIConfiguration = polygonAPIConfiguration;
    }

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public ResponseEntity<TickerDetails> getSymbolDetails(String symbol) {
        return restTemplate.exchange(
                polygonAPIConfiguration.getSymbolDetailsPath(),
                HttpMethod.GET,
                buildAuthorizationHeader(),
                TickerDetails.class,
                Map.of("symbol", symbol)
        );
    }

    @Override
    public ResponseEntity<StockDividends> getStockDividends(String symbol) {
        return restTemplate.exchange(
                polygonAPIConfiguration.getStockDividendsPath(),
                HttpMethod.GET,
                buildAuthorizationHeader(),
                StockDividends.class,
                Map.of("symbol", symbol)
        );
    }

    @Override
    public ResponseEntity<StockDailyOpenClose> getStockDailyOpenClose(String symbol, Date date, boolean adjusted) {
        return restTemplate.exchange(
                polygonAPIConfiguration.getStockDailyOpenClosePath(),
                HttpMethod.GET,
                buildAuthorizationHeader(),
                StockDailyOpenClose.class,
                Map.of(
                        "symbol", symbol,
                        "date", new SimpleDateFormat("yyyy-MM-dd").format(date),
                        "adjusted", adjusted
                )
        );
    }

    private HttpEntity buildAuthorizationHeader() {
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(polygonAPIConfiguration.getAuthorizationToken());
        return new HttpEntity<>(httpHeaders);
    }
}
