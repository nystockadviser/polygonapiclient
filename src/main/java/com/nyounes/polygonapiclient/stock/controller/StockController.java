package com.nyounes.polygonapiclient.stock.controller;

import com.nyounes.polygonapiclient.stock.model.StockDailyOpenClose;
import com.nyounes.polygonapiclient.stock.model.StockDividends;
import com.nyounes.polygonapiclient.stock.model.TickerDetails;
import com.nyounes.polygonapiclient.stock.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class StockController {
    private StockService stockService;

    @Autowired
    public void setTickerDetailsService(StockService stockService) {
        this.stockService = stockService;
    }

    @GetMapping("/{symbol}/details")
    public TickerDetails getSymbolDetails(@PathVariable String symbol){
        return stockService.getSymbolDetails(symbol);
    }

    @GetMapping("/{symbol}/dividends")
    public StockDividends getStockDividends(@PathVariable String symbol){
        return stockService.getStockDividends(symbol);
    }

    @GetMapping("/{symbol}/{date}")
    public StockDailyOpenClose getStockDividends(@PathVariable String symbol,
                                                 @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date date,
                                                 @RequestParam boolean adjusted){
        return stockService.getStockDailyOpenClose(symbol, date, adjusted);
    }
}
