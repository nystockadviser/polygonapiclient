package com.nyounes.polygonapiclient.stock.service.impl;

import com.nyounes.polygonapiclient.stock.client.StockClient;
import com.nyounes.polygonapiclient.stock.model.StockDailyOpenClose;
import com.nyounes.polygonapiclient.stock.model.StockDividends;
import com.nyounes.polygonapiclient.stock.model.TickerDetails;
import com.nyounes.polygonapiclient.stock.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;

@Service
public class StockServiceImpl implements StockService {
    private StockClient stockClient;

    @Autowired
    public void setTickerDetailsClient(StockClient stockClient) {
        this.stockClient = stockClient;
    }

    @Override
    public TickerDetails getSymbolDetails(String symbol) throws ResponseStatusException {
        ResponseEntity<TickerDetails> tickerDetailsResponseEntity = stockClient.getSymbolDetails(symbol);
        if(HttpStatus.OK.equals(tickerDetailsResponseEntity.getStatusCode())){
            return tickerDetailsResponseEntity.getBody();
        } else {
            throw new ResponseStatusException(tickerDetailsResponseEntity.getStatusCode());
        }
    }

    @Override
    public StockDividends getStockDividends(String symbol) throws ResponseStatusException {
        ResponseEntity<StockDividends> stockDividendsResponseEntity = stockClient.getStockDividends(symbol);
        if(HttpStatus.OK.equals(stockDividendsResponseEntity.getStatusCode())){
            return stockDividendsResponseEntity.getBody();
        } else {
            throw new ResponseStatusException(stockDividendsResponseEntity.getStatusCode());
        }
    }

    @Override
    public StockDailyOpenClose getStockDailyOpenClose(String symbol, Date date, boolean adjusted) throws ResponseStatusException {
        ResponseEntity<StockDailyOpenClose> stockDailyOpenCloseResponseEntity = stockClient.getStockDailyOpenClose(symbol, date, adjusted);
        if(HttpStatus.OK.equals(stockDailyOpenCloseResponseEntity.getStatusCode())){
            return stockDailyOpenCloseResponseEntity.getBody();
        } else {
            throw new ResponseStatusException(stockDailyOpenCloseResponseEntity.getStatusCode());
        }
    }
}
