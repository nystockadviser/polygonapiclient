package com.nyounes.polygonapiclient.stock.service;

import com.nyounes.polygonapiclient.stock.model.StockDailyOpenClose;
import com.nyounes.polygonapiclient.stock.model.StockDividends;
import com.nyounes.polygonapiclient.stock.model.TickerDetails;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;

public interface StockService {
    TickerDetails getSymbolDetails(String symbol) throws ResponseStatusException;
    StockDividends getStockDividends(String symbol) throws ResponseStatusException;
    StockDailyOpenClose getStockDailyOpenClose(String symbol, Date date, boolean adjusted);
}
