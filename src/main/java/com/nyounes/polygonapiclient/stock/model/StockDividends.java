package com.nyounes.polygonapiclient.stock.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class StockDividends {
    public int count;

    @JsonProperty("results")
    public List<DividendsResult> dividendsResults;

    public String status;
}
