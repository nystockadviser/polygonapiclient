package com.nyounes.polygonapiclient.stock.model;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class TickerDetails {
    public boolean active;
    public String bloomberg;
    public String ceo;
    public int cik;
    public String country;
    public String description;
    public int employees;
    public String exchange;
    public String exchangeSymbol;
    public Object figi;
    public String hq_address;
    public String hq_country;
    public String hq_state;
    public String industry;
    public String lei;
    public Date listdate;
    public String logo;
    public long marketcap;
    public String name;
    public String phone;
    public String sector;
    public int sic;
    public List<String> similar;
    public String symbol;
    public List<String> tags;
    public String type;
    public String updated;
    public String url;
}
