package com.nyounes.polygonapiclient.stock.model;

import lombok.Data;

@Data
public class StockDailyOpenClose {
    public String status;
    public String from;
    public String symbol;
    public int open;
    public double high;
    public double low;
    public double close;
    public int volume;
    public double afterHours;
    public double preMarket;
}
