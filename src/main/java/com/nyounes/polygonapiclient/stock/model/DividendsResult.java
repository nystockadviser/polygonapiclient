package com.nyounes.polygonapiclient.stock.model;

import lombok.Data;

import java.util.Date;

@Data
public class DividendsResult {
    public double amount;
    public Date exDate;
    public Date paymentDate;
    public Date recordDate;
    public String ticker;
}
