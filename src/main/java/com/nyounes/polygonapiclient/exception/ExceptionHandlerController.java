package com.nyounes.polygonapiclient.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ResponseStatusException.class)
    public String responseStatusException(ResponseStatusException responseStatusException, WebRequest request) {
        return String.format("%s: %s", responseStatusException.getRawStatusCode(), responseStatusException.getReason());
    }

    @ExceptionHandler(HttpClientErrorException.class)
    public String httpClientErrorException(HttpClientErrorException httpClientErrorException, WebRequest request) {
        return String.format("%s: %s", httpClientErrorException.getRawStatusCode(), httpClientErrorException.getCause());
    }
}
